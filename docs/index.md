# Python 단위 테스팅: Pytest, Unittest 라이브러리 <sup>[1](#footnote_1)</sup>

<font size="3">Python 프로젝트와 종속성을 격리하기 위한 가상 환경을 만들고 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./unit-testing.md#intro)
1. [단위 테스트란?](./unit-testing.md#sec_02)
1. [단위 테스트가 중요한 이유](./unit-testing.md#sec_03)
1. [Python에서 단위 테스트 작성 방법](./unit-testing.md#sec_04)
1. [Pytest: 기능과 사용법](./unit-testing.md#sec_05)
1. [Unittest 라이브러리: 기능과 사용법](./unit-testing.md#sec_06)
1. [Pytest와 Unittest 라이브러리 비교](./unit-testing.md#sec_07)
1. [Python에서 단위 테스트의 모범 사례](./unit-testing.md#sec_08)
1. [요약](./unit-testing.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 36 — Python Unit Testing: Pytest, Unittest Libraries](https://python.plainenglish.io/python-tutorial-36-python-unit-testing-pytest-unittest-libraries-a333c6d81c4d?sk=3cf324074ceac127a5c9df6c49b4a82d)를 편역하였습니다.
